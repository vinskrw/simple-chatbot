//FRONT-END SCRIPT
// const socket = io("http://localhost:3000");
const socket = io();
const messageContainer = document.getElementById("conversation-container");
const messageForm = document.getElementById("send-container");
const messageInput = document.getElementById("message-input");
const btnSendMessage = document.getElementById("chat-send");

socket.on("chat-message", (data) => {
  appendBotMessage(data.message);
  console.log(`botMsg:${data.message}`);  
});

socket.on("user-connected", (name) => {
  console.log(data);
});

socket.on("user-disconnected", (name) => {
  console.log(data);
});

socket.on('disconnect',(reason)=>{
  console.log('Server disconnected');
})

btnSendMessage.addEventListener("click", () => {
  const message = messageInput.value;
  appendUserMessage(message);
  socket.emit("send-chat-message", message);
  console.log(`userMsg:${message}`);
  messageInput.value = "";
});

messageInput.addEventListener("keypress", (e) => {
  const keyPressed = e.which || e.keyCode;
  if (keyPressed === 13) {
    const message = messageInput.value;
    appendUserMessage(message);
    socket.emit("send-chat-message", message);
    console.log(`userMsg:${message}`);
    messageInput.value = "";
  }
});

function appendUserMessage(message) {
  const date = new Date();
  const htmlResponse = `
  <div class="row message-body">\
    <div class="col-sm-12 message-main-sender">\
      <div class="sender">\
        <div class="message-text">${message}</div>\
        <span class="message-time pull-left">${date.getHours()}:${date.getMinutes()}</span>\
      </div>\
    </div>\
  </div>`;
  messageContainer.innerHTML = messageContainer.innerHTML + htmlResponse;
}

function appendBotMessage(message) {
  const date = new Date();
  if (message == "") {
    message = "You are not saying anything. Can I help you?";
  }
  const htmlResponse = `
  <div class="row message-body">\
    <div class="col-sm-12 message-main-receiver">\
      <div class="receiver">\
        <div class="message-text">${message}</div>\
        <span class="message-time pull-left">${date.getHours()}:${date.getMinutes()}</span>\
      </div>\
    </div>\
  </div>`;
  messageContainer.innerHTML = messageContainer.innerHTML + htmlResponse;
}

