//CLIENT SERVER PART
const express = require("express");
const app = express();
const http = require("http").createServer(app);
const port = process.env.PORT || 5000;
const chatBotManager = require("./bot_training");

app.use(express.static(__dirname)); //HTML Page

//Serve UI
app.get("/", (req, res) => {
  res.sendFile(__dirname + "index.html");
});

// SOCKET IO SERVER PART
const io = require("socket.io")(http, {
  pingTimeout: 5000,
  pingInterval: 5000,
});
const users = {};

io.on("connection", (socket) => {
  //Train the Pair first
  chatBotManager("");

  //Send a message after a timeout of 20 seconds
  // setTimeout(function () {
  //   socket.emit("chat-message", {
  //     message:
  //       "The chat will be end automatically if you do not send any message",
  //   });
  // }, 20000);

  let timer = null;

  socket.on("new-user", (name) => {
    users[socket.id] = name;
    socket.broadcast.emit("user-connected", name);
  });

  socket.on("send-chat-message", (message) => {
    chatBotManager(message).then((result) => {
      if (result != null) {
        socket.emit("chat-message", {
          message: result,
          name: users[socket.id],
        });
      } else {
        socket.emit("chat-message", {
          message: `Sorry I don't understand what you are saying.`,
          name: users[socket.id],
        });
      }
      //TIME OUT SETTING IF USER IDLE
      if (timer) {
        clearTimeout(timer);
        timer = null;
      }
      timer = setTimeout(() => {
        socket.emit("chat-message", {
          message:
            "The chat will be end automatically if you do not send any message",
        });
        setTimeout(() => {
          socket.emit("chat-message", {
            message:
              "The server is diconnected",
          });
          socket.disconnect(true)}
          , 10000);
      }, 60000);
    });

    // console.log(message);
  });

  socket.on("disconnect", (message) => {
    socket.broadcast.emit("user-disconnected", users[socket.id]);
    delete users[socket.id];
    // console.log(message);
  });
});

// app.listen(port, console.log(`Server started on port ${port}`));
//server start
http.listen(port, function () {
  console.log("Server started on port:5000");
});
