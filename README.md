
<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">simple-chatbot</h3>
</p>


<!-- TABLE OF CONTENTS -->
## Table of Contents

* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Using Docker](#using-docker)
* [License](#license)
* [Contact](#contact)



## Getting Started

<p align="center">
  This is a conversation agent or chatbot that returns an answer to a given question. The knowledge of the chatbot is based on a set of question and answer (QA) pairs. The general idea is that a given question will be answered using the answer of a similar question in QA pairs.

  To interact with the chatbot, users can submit their questions through a webpage. Some example question is listed as follows:
  * What is your you name details ?
  * What's the weather today?
  * economy news?
  * my name is %name%

  And the detail can be found in <a href="bot_training.js">bot_training.js</a>
</p>

### Installation
1. Install Dependencies
```
npm install
```

2. Compiles and run for development
```
npm run dev
```

### Using Docker

1. Building Images
```
docker build -t simple-chatbot
```

2. Build Container and Run it
```
docker run -it -p 5000:5000 simple-chatbot
```
## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Vinskrw - [@vinskrw]- vincentiuskurniawan95@gmail.com

Project Link: [https://gitlab.com/vinskrw/simple-chatbot](https://github.com/vinskrw/node-restful-api-sequelize)